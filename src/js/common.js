app.bind('default tabby', function(context){
    $('[js-tabby-tab]', context).tabby();
});

app.bind('default layer', function(context){
    $('[js-overlayer]', context).layer({
        isOverlayer: true,
        beforeOpen: ($layer, layerData) => {},
        afterOpen: ($layer, layerData) => {
            app.use('tabby', $layer);
            app.use('inputs', $layer);
        },
        afterClose: ($layer, layerData) => {}
    });
});


app.bind('default inputs', function(context){
    $('[data-inputmask]', context).inputmask({
        onincomplete: function () {
            $(this).trigger('maskIncomplete');
        }
    });
    $('[js-inputShadow]', context).inputShadow();
});

app.bind('default toggler', function(context){
    $('[js-toggler]', context).toggler();
});
