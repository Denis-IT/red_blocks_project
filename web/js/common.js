'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

!function (window, $, undefined) {
    'use strict';

    var App = function () {
        function App() {
            _classCallCheck(this, App);

            this.stack = [];
            this.started = false;
            this.sorted = false;
        }

        _createClass(App, [{
            key: 'use',
            value: function use(scope, context) {
                var _this = this;

                scope = App.normalizeScope(scope);

                if (!this.sorted) {
                    this.stack.sort(function (a, b) {
                        if (a.order === b.order) {
                            return 0;
                        }
                        return a.order > b.order ? 1 : -1;
                    });

                    this.sorted = true;
                }

                this.stack.filter(function (stack) {
                    return scope.some(function (scope) {
                        return stack.scope.indexOf(scope) > -1;
                    });
                }).map(function (stack) {
                    return stack.callback.call(_this, context, scope);
                });
            }
        }, {
            key: 'bind',
            value: function bind(scope, callback, order) {
                if (order === undefined) {
                    if (callback === undefined) {
                        callback = scope;
                        scope = ['default'];
                    } else if (typeof callback === 'number') {
                        order = callback || 0;
                        callback = scope;
                        scope = ['default'];
                    }
                }

                if (order === undefined) {
                    order = 0;
                }

                scope = App.normalizeScope(scope);

                this.stack.push({ scope: scope, callback: callback, order: order });
                this.sorted = false;
            }
        }, {
            key: 'start',
            value: function start() {
                var _this2 = this;

                if (this.started !== false) {
                    return;
                }

                this.started = true;

                $(function () {
                    _this2.use('default');
                });
            }
        }], [{
            key: 'normalizeScope',
            value: function normalizeScope(scope) {
                if (typeof scope === 'string') {
                    scope = scope.split(' ').map(function (scope) {
                        return scope.trim();
                    });
                }

                if (scope.length === 0) {
                    scope = ['default'];
                }

                return scope;
            }
        }]);

        return App;
    }();

    window.app = new App();
}(window, jQuery);
'use strict';

app.bind('default tabby', function (context) {
    $('[js-tabby-tab]', context).tabby();
});

app.bind('default layer', function (context) {
    $('[js-overlayer]', context).layer({
        isOverlayer: true,
        beforeOpen: function beforeOpen($layer, layerData) {},
        afterOpen: function afterOpen($layer, layerData) {
            app.use('tabby', $layer);
            app.use('inputs', $layer);
        },
        afterClose: function afterClose($layer, layerData) {}
    });
});

app.bind('default inputs', function (context) {
    $('[data-inputmask]', context).inputmask({
        onincomplete: function onincomplete() {
            $(this).trigger('maskIncomplete');
        }
    });
    $('[js-inputShadow]', context).inputShadow();
});

app.bind('default toggler', function (context) {
    $('[js-toggler]', context).toggler();
});
//# sourceMappingURL=common.js.map
